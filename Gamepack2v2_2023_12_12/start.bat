@echo off

rem Set the backup directory
set BackupDir=PlayerOut

rem Create the backup directory if it doesn't exist
if not exist %BackupDir% mkdir %BackupDir%

rem Copy files from Player1 to the backup directory
xcopy /s /y Player1\* %BackupDir%\

rem Copy files from src/main/java to Player1
xcopy /s /y BatBot\src\main\java\* Player1\

rem Compile Java files
javac Player1\*.java
javac Player2\*.java
javac Player3\*.java
javac Player4\*.java

rem Run the Java program
java -jar Game.jar -retina=false Player1 Player2 Player3 Player4
