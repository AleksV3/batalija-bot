import { Component } from "react";
import { ABOUT, NOVICE, ADDNEW, SIGNUP, LOGIN, NOVICA, HOME, LOGOUT } from "./Utils/Constants"
import HomeView from "./CustomComponents/HomeView";
import AboutView from "./CustomComponents/AboutView";
import NoviceView from "./CustomComponents/NoviceView";
import AddNovicaView from "./CustomComponents/AddNovicaView";
import SignupView from "./CustomComponents/SignupView";
import LoginView from "./CustomComponents/LoginView";
import SingleNovicaView from "./CustomComponents/SingleNovicaView";
import axios from "axios";
import { API_URL } from "./Utils/Configuration";
import Cookies from "universal-cookie";
let cookies = new Cookies();


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      CurrentPage: HOME,
      Novica: 1,
      status: {
        success: null,
        msg: ""
      },
      user: null
    };    
    if (cookies.get('user_name') != null && this.state.user == null) {
      this.state.CurrentPage = LOGIN
    }
  }


  
  QGetView(state) {
    const page = state.CurrentPage;
    switch (page) {
      case ABOUT:
        return <AboutView />;
      case NOVICE:
        return <NoviceView QIDFromChild={this.QSetView} />;
      case ADDNEW:
        return <AddNovicaView />;
      case SIGNUP:
        return <SignupView />;
      case LOGIN:
        return <LoginView QUserFromChild={this.QSetLoggedIn} />;
      case LOGOUT:
        return <HomeView />;
      case NOVICA:
        return <SingleNovicaView data={state.Novica} QIDFromChild={this.QSetView} />;
      default:
        return <HomeView />;
    }
  };

  QSetView = (obj) => {
    this.setState(this.state.status = { success: null, msg: "" })

    console.log("QSetView");
    this.setState({
      CurrentPage: obj.page,
      Novica: obj.id || 0
    });
  };

  QSetLoggedIn = (obj) => {
    this.setState(this.state.user = obj.user);
  }

  QLogOut = () => {

    cookies.remove("user_name");
    cookies.remove("user_passward");

    let req = axios.create({
      timeout: 2000,
      withCredentials: true
    });
    
    req.get(API_URL + '/users/logout').then(response => {
        this.setState(this.state.status = response.data);
        this.setState(this.state.user = null);
    });
  }
  render() {
    return (
      <div id="APP" className="container">
        <div id="menu" className="row">
          <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
            <div className="container-fluid">
              <a onClick={this.QSetView.bind(this, { page: "home" })}className="navbar-brand"href="#"> Home</a>
              <button
                className="navbar-toggler"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="navbar-toggler-icon"></span>
              </button>

              <div
                className="collapse navbar-collapse"
                id="navbarSupportedContent"
              >
                <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                  <li className="nav-item">
                    <a
                      // onClick={() => this.QSetView({ page: ABOUT })}
                      onClick={this.QSetView.bind(this, { page: ABOUT })}
                      className="nav-link " href="#">About</a>
                  </li>

                  <li className="nav-item">
                    <a
                      // onClick={() => this.QSetView({ page: NOVICE })}
                      onClick={this.QSetView.bind(this, { page: NOVICE })}
                      className="nav-link "
                      href="#"
                    >
                      News
                    </a>
                  </li>

                  <li className="nav-item">
                    <a
                      //onClick={() => this.QSetView({ page: ADDNEW })}
                      onClick={this.QSetView.bind(this, { page: ADDNEW })}
                      className="nav-link"
                    >
                      Add news
                    </a>
                  </li>

                  <li className="nav-item">
                    <a
                      //onClick={() => this.QSetView({ page: SIGNUP })}
                      onClick={this.QSetView.bind(this, { page: SIGNUP })}
                      className="nav-link " href="#">Sign up</a>
                  </li>
                  {this.state.user ?
                 <li className="nav-item" ><a onClick={() => {
                      this.QSetView({page: HOME });
                        this.QLogOut();
                 }}
                      className="nav-link " href="#"> Logout </a>
                 </li>
                 :
                 <li className="nav-item" ><a onClick={this.QSetView.bind(this, { page: LOGIN })}
                      className="nav-link " href="#"> Login </a>
                 </li>
                }
                </ul>
              </div>
            </div>
          </nav>
        </div>

        <div id="viewer" className="row container">
          {this.QGetView(this.state)}
          {this.state.status.success ?
            <p className="alert alert-success"
              role="alert">{this.state.status.msg}</p> : null}
        </div>
      </div>
    );
  }
}

export default App;
