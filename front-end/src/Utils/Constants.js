export const ABOUT = 'about';
export const NOVICE = 'novice';
export const ADDNEW = 'addnew';
export const SIGNUP = 'signup';
export const LOGIN = 'login';
export const LOGOUT = 'logout';
export const NOVICA = 'novica';
export const HOME = 'home';